package com.imanancin.mydevapp

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class Adapter(
    val context: Context
) : ListAdapter<AppCrash, Adapter.ViewHolder>(DIFF)  {
    object DIFF : DiffUtil.ItemCallback<AppCrash>() {
        override fun areItemsTheSame(oldItem: AppCrash, newItem: AppCrash): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: AppCrash, newItem: AppCrash): Boolean {
            return oldItem == newItem
        }

    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private var appname: TextView
        private var date: TextView

        init {
            appname = view.findViewById(R.id.appname)
            date = view.findViewById(R.id.date)
        }

        fun bind(position: AppCrash) {
            appname.text = position.appname
            date.text = position.date
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, CrashActivity::class.java)
                intent.putExtra("data", position)
                itemView.context.startActivity(intent)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_app, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }


}