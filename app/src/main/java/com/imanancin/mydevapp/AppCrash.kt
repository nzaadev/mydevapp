package com.imanancin.mydevapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AppCrash(
    val id: Int,
    val appname: String,
    val date: String,
    val data1: String,
    val data2: String
) : Parcelable
