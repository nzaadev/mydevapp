package com.imanancin.mydevapp

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class CrashActivity : AppCompatActivity() {

    private lateinit var dataaa: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crash)

        val intent = intent.getParcelableExtra<AppCrash>("data")
        val data = findViewById<TextView>(R.id.data)
        val dataa = findViewById<TextView>(R.id.dataa)
        val date = findViewById<TextView>(R.id.dates)
        val app = findViewById<TextView>(R.id.app)
        if(intent != null ) {
            data.text = intent.data2
            dataa.text = intent.data1
            date.text = intent.date
            app.text = intent.appname

            dataaa = intent.data1 + "\n\n" + intent.data2
        }



    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.share -> {
                val intent = Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, dataaa)
                    putExtra(Intent.EXTRA_SUBJECT, "Share error")
                }
                startActivity(Intent.createChooser(intent, "Share using"))
                return true
            }

            R.id.about -> {
                Toast.makeText(this@CrashActivity, "By Imanz", Toast.LENGTH_SHORT).show()
                return true
            }
        }
        return true
    }


}