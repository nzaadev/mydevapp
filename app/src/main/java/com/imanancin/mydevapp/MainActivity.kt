package com.imanancin.mydevapp

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    private lateinit var repository: Repository


    @SuppressLint("SdCardPath")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val activity = this
        repository = Repository()
        val wakeLock = findViewById<SwitchCompat>(R.id.wakeLock)

        val foregroundServiceIntent = Intent(this, MyService::class.java)
        val serviceRunning = checkServiceRunning(MyService::class.java)
        data.value = serviceRunning

        data.observe(this) {
            wakeLock.isChecked = it
        }

        wakeLock.setOnCheckedChangeListener { _, b ->
            if(b) {
                data.value = true
                startForegroundService(foregroundServiceIntent)
                Toast.makeText(this, "Wakelock active", Toast.LENGTH_SHORT).show()
            } else {
                data.value = false
                stopService(foregroundServiceIntent)
                Toast.makeText(this, "Wakelock stopped", Toast.LENGTH_SHORT).show()
            }
        }


        val adapter = Adapter(this)
        val rv = findViewById<RecyclerView>(R.id.rv)
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = adapter
        rv.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL).apply {
            setDrawable(ColorDrawable(resources.getColor(R.color.white)))
        })
        adapter.submitList(repository.tokenizer())

    }

    private fun checkServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    companion object {
        var data: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    }



}