package com.imanancin.mydevapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val foregroundServiceIntent = Intent(context, MyService::class.java)
        context.stopService(foregroundServiceIntent)
        Toast.makeText(context, "Service stopped", Toast.LENGTH_SHORT).show()
        MainActivity.data.value = false
    }
}