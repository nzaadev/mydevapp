package com.imanancin.mydevapp

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import androidx.core.app.NotificationCompat

class MyService : Service() {

    private lateinit var powerManager: PowerManager
    private lateinit var wakeLock: PowerManager.WakeLock

    override fun onBind(intent: Intent): IBinder {
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun onCreate() {
        powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        wakeLock = powerManager.newWakeLock(10, "Wakelock: wakelock")
        wakeLock.acquire(10*60*1000L /*10 minutes*/)
        startForeground(ID_NOTIF, doNotification())
    }

    private fun createNotificationBuilder(): NotificationCompat.Builder {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
            val notification = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (notification.getNotificationChannel(NOTIFICATION_CHANNEL) == null) {
                val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_NAME, NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.description = "service background notification"
                notification.createNotificationChannel(notificationChannel)
            }

            return NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL).apply {
                setDefaults(Notification.DEFAULT_SOUND + Notification.DEFAULT_VIBRATE)
                priority = Notification.PRIORITY_MAX// h
            }
        }
        return NotificationCompat.Builder(applicationContext).apply {
            setDefaults(Notification.DEFAULT_SOUND + Notification.DEFAULT_VIBRATE)
            priority = Notification.PRIORITY_MAX// h
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }


    private fun doNotification(): Notification {
        val intent = Intent(applicationContext, MainActivity::class.java)
        val intentAction = Intent(applicationContext, MyReceiver::class.java)

        val notificationBuilder = createNotificationBuilder().apply {
            setSmallIcon(R.mipmap.ic_launcher)
            setContentTitle("Device wake...")
            setOngoing(true)
            setContentIntent(PendingIntent.getActivity(applicationContext, 0, intent, 0))
            priority = NotificationCompat.PRIORITY_HIGH
            addAction(R.drawable.ic_baseline_stop_24, "Stop", PendingIntent.getBroadcast(applicationContext, 0, intentAction, 0))
        }

        return notificationBuilder.build()
    }


    companion object {
        const val NOTIFICATION_CHANNEL = "mynotif"
        const val NOTIFICATION_NAME = "mynotifApp"
        const val ID_NOTIF = 212
    }
}