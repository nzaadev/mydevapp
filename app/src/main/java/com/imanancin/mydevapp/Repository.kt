package com.imanancin.mydevapp

import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class Repository {

    private val delime = "========================================"

    fun tokenizer(): ArrayList<AppCrash> {
        var num = 1
        val arr = ArrayList<AppCrash>()
        val a = StringTokenizer(RunCommand("dumpsys dropbox --print data_app_crash"), delime)
        a.nextToken()
        a.nextToken()
        while (a.hasMoreTokens()) {

            val token = a.nextToken()
            val txt = token.split("\n\n") // split statistic and error data txt[0] statistic
            val line = txt[0].split("\n")
            val regex = "^Package: (.*)$"
            val pattern: Pattern = Pattern.compile(regex, Pattern.MULTILINE)
            val matcher: Matcher = pattern.matcher(txt[0])
            if(matcher.find()) {
                matcher.group(1)?.let { arr.add(AppCrash(num, it, line[1].split(" ")[0],txt[0], txt[1])) }
            }
        }

        arr.reverse()
        return arr
    }

    private fun extractInfo(pattern: String, string: String): String? {
        val patterns = Pattern.compile(pattern, Pattern.DOTALL)
        val matcher = patterns.matcher(string)
        return if(matcher.find()) {
            matcher.group(1)
        } else {
            null
        }
    }


    fun RunCommand(cmd: String?): String {
        val cmdOut = StringBuffer()
        val process: Process
        try {
            process = Runtime.getRuntime().exec(cmd)
            val r = InputStreamReader(process.inputStream)
            val bufReader = BufferedReader(r)
            val buf = CharArray(4096)
            var nRead = 0
            while (bufReader.read(buf).also { nRead = it } > 0) {
                cmdOut.append(buf, 0, nRead)
                Log.e("ERR", buf.toString() )
            }
            bufReader.close()
            try {
                process.waitFor()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return cmdOut.toString()
    }

}